#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#include "add.hh"

int main(const int argc, const char *const *argv)
{
    if (argc < 2) {
        std::cout << "Not enough arguments to continue test.\n";
        return 1;
    }
    
    int expected_return_code;
    auto ss = std::stringstream();
    ss << argv[1];
    if (!(ss >> expected_return_code)) {
        ss.clear();
        std::cout << "Unable to convert return code argument to an integer.\n";
        return 1;
    }

    auto args = std::vector<std::string>();
    for (int i = 1; i < argc; i++) {
        args.emplace_back(argv[i]);
    }

    // if (cmd::add(args) == expected_return_code && expected_return_code == 1) {
    //     return 0;
    // }

    // if (cmd::add(args) != expected_return_code) {
    //     return 1;
    // }

    return 0;
}
