#include <algorithm>
#include <experimental/filesystem>
#include <iostream>
#include <list>
#include <set>
#include <string>
#include <vector>

#include "doctor.hh"
#include "nlohmann/json.hpp"
#include "utility.hh"

namespace fs = std::experimental::filesystem;
using json   = nlohmann::json;

// the cout queue
auto error_list = std::list<std::string>();

void doctor::check_duplicate_names(json &j) noexcept
{
    // Checking for duplicate names
    auto duplicate_set = std::set<std::string>();
    auto unique_names  = std::list<std::string>();
    for (const json &distro : j["distributions"]) {
        std::string name = distro["name"];
        if (std::find(unique_names.begin(), unique_names.end(), name) == unique_names.end()) {
            unique_names.push_back(name);
            continue;
        }
        duplicate_set.insert(name);
    }

    // Join all duplicate names into one string, then print
    if (!duplicate_set.empty()) {
        error_list.push_back("Duplicate name(s) found: " + util::container_2_string(duplicate_set) +
                             '.');
    }
}

void doctor::check_duplicate_paths(json &j) noexcept
{
    // Checking for duplicate paths
    auto duplicate_set = std::set<std::string>();
    auto unique_paths  = std::list<std::string>();
    for (const json &distro : j["distributions"]) {
        std::string path = distro["path"];
        if (std::find(unique_paths.begin(), unique_paths.end(), path) == unique_paths.end()) {
            unique_paths.push_back(path);
            continue;
        }
        duplicate_set.insert(path);
    }

    // Join all duplicate paths into one string, then print
    if (!duplicate_set.empty()) {
        error_list.push_back("Duplicate path(s) found: " + util::container_2_string(duplicate_set) +
                             '.');
    }
}

void doctor::check_duplicate_sets(json &j) noexcept
{
    int number_of_sets = 0;
    for (const json &distro : j["distributions"]) {
        if (distro["set"]) {
            number_of_sets++;
        }
    }

    if (number_of_sets > 1) {
        error_list.push_back("Multiple sets detected.");
    }
}

void doctor::check_json_schema(json &j) noexcept
{
    // Root level type check
    if (!j.is_object()) {
        std::cout << "Root level JSON is not an object.\n"
                  << "Please run 'java-env-manager doctor -f' to correct the problem(s).\n";
        exit(1);
    }

    // Root level key checks
    auto root_keys = std::vector<std::string>();
    for (json::const_iterator it = j.begin(); it != j.end(); it++) {
        root_keys.push_back(it.key());
    }

    if (root_keys.size() == 0) {
        std::cout << "No 'distribution key' is located at root level.\n"
                  << "Please run 'java-env-manager doctor -f' to correct the problem(s).\n";
        exit(1);
    } else if (root_keys.size() > 1) {
        std::cout << "Only key allowed at root level is 'distributions'. Currently have "
                  << util::container_2_string(root_keys) << ".\n"
                  << "Please run 'java-env-manager doctor -f' to correct the problem(s).\n";
        exit(1);
    } else {
        if (root_keys[0].compare("distributions") != 0) {
            std::cout << "Only key allowed at root level is 'distributions'. Currently have "
                      << util::container_2_string(root_keys) << ".\n"
                      << "Please run 'java-env-manager doctor -f' to correct the problem(s).\n";
            exit(1);
        }
    }

    const auto distributions = j["distributions"];

    // distributions type check
    if (!distributions.is_array()) {
        std::cout << "Value of 'distributions' is not an array.\n"
                  << "Please run 'java-env-manager doctor -f' to correct the problem(s).\n";
        exit(1);
    }

    // No sense in checking keys if size is equal to 0
    if (distributions.size() == 0) {
        return;
    }

    // distribution object key checks
    auto invalid_keys = std::set<std::string>();
    for (const json &distro : distributions) {
        // retrieve all keys from distro
        for (json::const_iterator it = distro.begin(); it != distro.end(); it++) {
            std::string key = it.key();
            if (key.compare("name") != 0 && key.compare("path") != 0 && key.compare("set") != 0) {
                invalid_keys.insert(key);
            }
        }
    }

    // Printing help statement
    if (!invalid_keys.empty()) {
        std::cout << "Invalid key(s) located in " << SETTINGS_FILE << ": "
                  << util::container_2_string(invalid_keys) << ".\n";
    }

    // Check value types for proper keys
    bool name_not_string = false;
    bool path_not_string = false;
    bool set_not_bool    = false;
    for (const json &distro : distributions) {
        if (!distro["name"].is_string() && !name_not_string) {
            name_not_string = true;
        }

        if (!distro["path"].is_string() && !path_not_string) {
            path_not_string = true;
        }

        if (!distro["set"].is_boolean() && !set_not_bool) {
            set_not_bool = true;
        }
    }

    if (name_not_string) {
        std::cout << "Value(s) of key 'name' should be of type string.\n";
    }

    if (path_not_string) {
        std::cout << "Value(s) of key 'path' should be of type string.\n";
    }

    if (set_not_bool) {
        std::cout << "Value(s) of key 'set' should be of type boolean.\n";
    }

    if (!invalid_keys.empty() || name_not_string || path_not_string || set_not_bool) {
        std::cout << "Please run 'java-env-manager doctor -f' to correct the problem(s).\n";
        exit(1);
    }
}

void doctor::check_paths_exist(json &j) noexcept
{
    auto dont_exist               = std::list<std::string>();
    auto not_directory_or_symlink = std::list<std::string>();
    for (const json &distro : j["distributions"]) {
        std::string path = distro["path"];
        if (!fs::exists(path)) {
            dont_exist.push_back(path);
        } else {
            if (!fs::is_directory(path) && !fs::is_symlink(path)) {
                not_directory_or_symlink.push_back(path);
            }
        }
    }

    if (!dont_exist.empty()) {
        error_list.push_back(
            "The following paths don't exist: " + util::container_2_string(dont_exist) + '.');
    }

    if (!not_directory_or_symlink.empty()) {
        error_list.push_back("The following paths are neither directories or symlinks: " +
                             util::container_2_string(not_directory_or_symlink) + '.');
    }
}

void cmd::doctor(std::vector<std::string> args, bool debug) noexcept
{
    json j = util::get_settings();

    // Run checks
    // doctor::check_json_schema(j); // Ran in util::get_settings
    doctor::check_duplicate_names(j);
    doctor::check_duplicate_paths(j);
    doctor::check_paths_exist(j);
    if (error_list.size() > 0) {
        for (const std::string &error : error_list) {
            std::cout << error << '\n';
        }
        std::cout << "Please run 'java-env-manager doctor -f' to correct the problem(s).\n";
    }
}
