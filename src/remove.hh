#ifndef _REMOVE_HH_
#define _REMOVE_HH_

#include <string>
#include <vector>

namespace cmd
{
    void remove(std::vector<std::string> args, bool debug = false) noexcept;
}

#endif    // _REMOVE_HH_
