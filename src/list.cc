#include <iostream>
#include <map>
#include <stdexcept>
#include <string>
#include <vector>

#include "list.hh"
#include "nlohmann/json.hpp"
#include "utility.hh"

void cmd::list(std::vector<std::string> args, bool debug) noexcept
{
    if (args.size() != 1) {
        std::cout << "Too many arguments.\n";
        exit(1);
    }

    const json j = util::get_settings();

    std::string prepend;
    for (const json &distro : j["distributions"]) {
        if (distro["set"].get<bool>()) {
            prepend = "* ";
        } else {
            prepend = "  ";
        }
        std::cout << prepend << distro["name"].get<std::string>() << '\n';
    }
}
