#ifndef _ADD_HH_
#define _ADD_HH_

#include <string>
#include <vector>

namespace cmd
{
    void add(std::vector<std::string> args, bool debug = false) noexcept;
}

#endif    // _ADD_HH_
