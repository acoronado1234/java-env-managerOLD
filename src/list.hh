#ifndef _LIST_HH_
#define _LIST_HH_

#include <string>
#include <vector>

namespace cmd
{
    void list(std::vector<std::string> args, bool debug = false) noexcept;
}

#endif    // _LIST_HH_
