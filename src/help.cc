#include <functional>
#include <iostream>
#include <map>
#include <string>
#include <vector>

#include "help.hh"

void cmd::help(std::vector<std::string> args, bool debug) noexcept
{
    if (args.size() <= 1) {
        help::help();
    } else if (args.size() == 2) {
        // Easter Egg :)
        if (args[1].compare("help") == 0) {
            std::cout << "'\"Patience you must have, my young padawan.\"\n\n-- Master Yoda'\n\n-- "
                         "Michael Scott\n";
            return;
        }

        // mapping each argument to their print functions
        const std::map<std::string, std::function<void()>> extra_help = {
            {"add", help::add},        {"doctor", help::doctor}, {"help", help::help},
            {"init", help::init},      {"list", help::list},     {"remove", help::remove},
            {"set", help::set},        {"which", help::which},   {"update", help::update},
            {"version", help::version}};

        // find and run print function
        auto temp = extra_help.find(args[1]);
        if (temp != extra_help.end()) {
            temp->second();
        } else {
            std::cout << args[1] << " not a valid subcommand\n";
            help::help();
        }
    } else {
        std::cout << "Too many arguments.\n";
        help::help();
    }
}

void help::add() noexcept
{
    std::cout << "Usage: java-env-manager add [name] [path]\n\n"
              << "\tname is what you want to reference the path by\n"
              << "\tpath is the path of the Java distribution you want to add\n\n"
              << "Adds a Java distribution to pick from\n";
}

void help::doctor() noexcept
{
    std::cout << "Usage: java-env-manager doctor [options]\n\n"
              << "Checks for problems with the settings.json file\n\n"
              << "Options:\n"
              << "  -f, --fix\t Applies fixes to the settings.json file in order for it to become "
                 "usable\n";
}

void help::help() noexcept
{
    std::cout << "Usage: java-env-manager help [options] [commands] [arguments]\n\n"
              << "Commands include:\n"
              << "  add\t\tAdd a Java distribution\n"
              << "  doctor\tChecks for problems with the settings.json file\n"
              << "  help\t\tPrint the help command\n"
              << "  init\t\tInitialize the Java Environment Manager\n"
              << "  list\t\tList all known Java distributions\n"
              << "  remove\tRemove a Java distribution\n"
              << "  set\t\tSet the current Java distribution\n"
              << "  update\tUpdate the path a name points to\n"
              << "  version\tPrint the name of the current Java distribution\n"
              << "  which\t\tPrint the path to the current Java executables\n\n"
              << "Options include:\n"
              << "  -d, --debug\tAdd debug logging to commands\n"
              << "  -v, --version\tPrint the version of the Java Environment Manager\n";
}

void help::init() noexcept
{
    std::cout << "Usage: java-env-manager init\n\n"
              << "Intializes the Java Environment Manager\n";
}

void help::list() noexcept
{
    std::cout << "Usage: java-env-manager list\n\n"
              << "Lists all known Java distributions\n";
}

void help::remove() noexcept
{
    std::cout << "Usage: java-env-manager add [name]\n"
              << "  name: the Java distribution you want to remove\n\n"
              << "Removes a Java distribution\n";
}

void help::set() noexcept
{
    std::cout << "Usage: java-env-manager set [name]\n\n"
              << "  name: the Java distribution to set as active\n\n"
              << "Sets the current Java distribution\n";
}

void help::update() noexcept
{
    std::cout << "Usage: java-env-manager update [name] [path]\n\n"
              << "  name: the Java distribution to update\n"
              << "  path: the new path of the Java distribution\n\n"
              << "Updates the path a name points to\n";
}

void help::version() noexcept
{
    std::cout << "Usage: java-env-manager version\n\n"
              << "Prints the name of the current Java distribution\n";
}

void help::which() noexcept
{
    std::cout << "Usage: java-env-manager version\n\n"
              << "Prints the path to the current Java executables\n";
}
