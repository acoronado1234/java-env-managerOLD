#include <cstdlib>
#include <experimental/filesystem>
#include <fstream>
#include <iostream>
#include <memory>
#include <pwd.h>
#include <stdexcept>
#include <string>
#include <sys/types.h>
#include <unistd.h>

#include "doctor.hh"
#include "nlohmann/json.hpp"
#include "utility.hh"

namespace fs = std::experimental::filesystem;
using json   = nlohmann::json;

/*
 * Returns the Java Environment Manager home directory
 */
const std::string util::get_program_dir() noexcept
{
    std::string program_dir;
    const auto JAVA_ENV_MANAGER_HOME =
        std::make_unique<const char *>(std::getenv("JAVA_ENV_MANAGER_HOME"));
    if (*JAVA_ENV_MANAGER_HOME) {
        program_dir = *JAVA_ENV_MANAGER_HOME;
    } else {
        const std::unique_ptr<const char *> HOME =
            std::make_unique<const char *>(std::getenv("HOME"));
        if (*HOME) {
            program_dir = std::string(*HOME) + FOLDER_NAME;
        } else {
            program_dir = std::string(getpwuid(getuid())->pw_dir) + FOLDER_NAME;
        }
    }

    return program_dir.find_last_of('/') == program_dir.size() - 1 ? program_dir
                                                                   : program_dir + '/';
}

/*
 * Returns the Java Environment Manager bin directory
 */
const std::string util::get_program_bin_dir(std::string program_dir) noexcept
{
    return (program_dir.empty() ? get_program_dir() : program_dir) + "bin";
}

/*
 * Returns the JSON representation of the settings file
 */
json util::get_settings(std::string program_dir)
{
    if (program_dir.empty()) {
        program_dir = util::get_program_dir();
    }

    std::string settings_file_path = program_dir + SETTINGS_FILE;

    if (!fs::exists(settings_file_path)) {
        std::cout << settings_file_path + " does not exist. Unable to retrieve settings.\n";
        exit(1);
    }

    json j;
    std::ifstream isettings = std::ifstream(settings_file_path);
    isettings >> j;
    isettings.close();

    doctor::check_json_schema(j);

    return j;
}

/*
 * Sets the settings using a JSON object
 */
void util::set_settings(json &j, std::string program_dir) noexcept
{
    if (program_dir.empty()) {
        program_dir = util::get_program_dir();
    }

    std::ofstream osettings = std::ofstream(program_dir + SETTINGS_FILE);
    osettings << std::setw(4) << j << std::endl;
    osettings.close();
}
