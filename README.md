# Java Environment Manager

This program is meant to allow ease of use when switching between various Java
distributions.

## How it Works

A Java distribution comes packaged like this:

```text
jdk/
├── bin
│   └── ...
├── conf
│   └── ...
├── include
│   └── ...
├── jmods
│   └── ...
├── legal
│   └── ...
├── lib
│   └── ...
├── man
│   └── ...
└── release
```

When you install the distrbution through your package manager, your executables
can get overwritten if you have both JDK 7 and JDK 8 installed. Java Environment
Manager will create symlinks to the binaries which should be located in something
akin to `/usr/lib64/openjdk-8/bin` into `JAVA_ENV_MANAGER_HOME/bin`. From there
the `JAVA_ENV_MANAGER_HOME/bin` will need to be added to your `PATH` before
`/usr/bin`. Now it is off to the races. You can change distributions
dynamically on a project-by-project basis.

## How to Build

### Dependencies

* `meson` (Project is currently built using `>= 0.42.1`. Not sure of the minimum
version it can be built with)
* `C++ >= C++17` (Liberal usage of `std::filesystem`)
    * Willing to accept merge requests to backport to older C++ standards no older than C++11
* `clang++` or `g++` (Not sure of minimum versions. I use `g++ >= 7 / clang++ >= 5`)
* Third Party
    * https://github.com/nlohmann/json (`json.hpp` located in tree)

### Commands

```text
CXX=[C++ compiler] meson build
cd build/
ninja
```

Executable is located at `src/java-env-manager`.

```text
Usage: java-env-manager help [options] [commands] [arguments]

Commands include:
  add           Add a Java distribution
  doctor        Checks for problems with the settings.json file
  help          Print the help command
  init          Initialize the Java Environment Manager
  list          List all known Java distributions
  remove        Remove a Java distribution
  set           Set the current Java distribution
  update        Update the path a name points to
  version       Print the name of the current Java distribution
  which         Print the path to the current Java executables

Options include:
  -d, --debug   Add debug logging to commands
  -v, --version Print the version of the Java Environment Manager
```

## Testing

### Running Tests

```text
cd build/
ninja test
```

### Developing Tests

Each command of `java-env-manager` will have its own set of tests located within `tests/[command].cc`.

For instance, here is a possible `add.cc` test file:

```c++
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#include "add.hh"

int main(const int argc, const char *const *argv)
{
    if (argc < 2) {
        std::cout << "Not enough arguments to continue test.\n";
        return 1;
    }

    int expected_return_code;
    auto ss = std::stringstream();
    ss << argv[1];
    if (!(ss >> expected_return_code)) {
        ss.clear();
        std::cout << "Unable to convert return code argument to an integer.\n";
        return 1;
    }

    auto args = std::vector<std::string>();
    for (int i = 1; i < argc; i++) {
        args.emplace_back(argv[i]);
    }

    if (cmd::add(args) == expected_return_code && expected_return_code == 1) {
        return 0;
    }

    if (cmd::add(args) != expected_return_code) {
        return 1;
    }

    return 0;
}
```

Adding to `tests/meson.build`:

```text
add = executable(
    'add',
    sources: [sources, 'add.cc', '../src/add.cc'],
    include_directories: include_dirs
)
test('ADD: No args', add, args: ['1'])
test('ADD: One arg', add, args: ['1', 'name'])
test('ADD: More than two args', add, args: ['1', 'name', 'dir', 'extra'])
```

Find more information [here](http://mesonbuild.com/Unit-tests.html).

## Roadmap to 1.0

1. Creating the `doctor` command
2. Complete unit testing
3. Set up logging

## Contributors

* @acoronado1234
